package pages;

import wrappers.Annotations;

public class CreateLeadPage extends Annotations {
	
	public CreateLeadPage enterCompanyName(String data) {
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String data) {
		  driver.findElementById("createLeadForm_firstName").sendKeys(data);   
		  return this;
	}
	public CreateLeadPage enterLastName(String data) {
		  driver.findElementById("createLeadForm_lastName").sendKeys(data);   
		  return this;
	}
	
	public MyViewLeadPage clickCreateLeadButton() {
		driver.findElementByName("submitButton").click();
		return new MyViewLeadPage();
	}
	
	
	
	
	

	

}
