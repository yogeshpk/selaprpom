package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC002CreateLead extends Annotations {
	
	@BeforeTest
	public void setData() {
		excelFileName = "TC002";
	}
	
	@Test(dataProvider = "fetchData")
	public void loginLogout(String userName, String password, String logInName, String cname, String fname, String lname, String fName) {
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLoginButton()
		.verifyLoginName(logInName)
		.clickCrmsfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(cname)
		.enterFirstName(fname)
		.enterLastName(lname)
		.clickCreateLeadButton()
		.verifyFirstName(fName);
	}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

